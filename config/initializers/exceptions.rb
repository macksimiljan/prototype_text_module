module Exceptions
  # for ExtractParser
  class InvalidExtractFile < RuntimeError
    def initialize(msg = 'Something went wrong when parsing the extract file.')
      super(msg)
    end
  end
end