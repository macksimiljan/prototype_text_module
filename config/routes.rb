Rails.application.routes.draw do
  resources :comments do
    collection do
      get :toggle_please_check
    end
  end
  resources :attestations
  resources :users
  resources :extracts do
    member do
      get :check_upload
    end
  end

  get 'text_dashboard', to: 'dashboards#text_module'
  resources :dashboards do
    collection do
      post :upload_and_parse
    end
  end


  root to: 'dashboards#text_module'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
