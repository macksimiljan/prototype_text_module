# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_16_083826) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attestations", force: :cascade do |t|
    t.string "orthography", null: false
    t.string "encoding"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "created_by_id"
    t.bigint "updated_by_id"
    t.bigint "extract_id"
    t.index ["created_by_id"], name: "index_attestations_on_created_by_id"
    t.index ["extract_id"], name: "index_attestations_on_extract_id"
    t.index ["updated_by_id"], name: "index_attestations_on_updated_by_id"
  end

  create_table "category_comments", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string "field"
    t.text "body", null: false
    t.string "commentable_type"
    t.bigint "commentable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "created_by_id"
    t.bigint "updated_by_id"
    t.bigint "category_comment_id"
    t.index ["category_comment_id"], name: "index_comments_on_category_comment_id"
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    t.index ["created_by_id"], name: "index_comments_on_created_by_id"
    t.index ["updated_by_id"], name: "index_comments_on_updated_by_id"
  end

  create_table "extracts", force: :cascade do |t|
    t.text "translation"
    t.string "manuscript_position"
    t.string "text_position"
    t.string "edition_position", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "created_by_id"
    t.bigint "updated_by_id"
    t.boolean "unchecked_quote", default: false
    t.string "quote_origin"
    t.string "quote_type"
    t.boolean "checked_upload", default: false, null: false
    t.xml "coptic"
    t.index ["created_by_id"], name: "index_extracts_on_created_by_id"
    t.index ["updated_by_id"], name: "index_extracts_on_updated_by_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "code", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "attestations", "extracts"
  add_foreign_key "attestations", "users", column: "created_by_id"
  add_foreign_key "attestations", "users", column: "updated_by_id"
  add_foreign_key "comments", "category_comments"
  add_foreign_key "comments", "users", column: "created_by_id"
  add_foreign_key "comments", "users", column: "updated_by_id"
  add_foreign_key "extracts", "users", column: "created_by_id"
  add_foreign_key "extracts", "users", column: "updated_by_id"
end
