class AddNotNullConstraints < ActiveRecord::Migration[5.2]
  def change
    change_column_null :attestations, :encoding, false
    change_column_null :attestations, :orthography, false
    change_column_null :category_comments, :name, false
    change_column_null :comments, :body, false
    change_column_null :extracts, :coptic, false
    change_column_null :users, :code, false
  end
end
