class AddCategoryCommentToComments < ActiveRecord::Migration[5.2]
  def change
    add_reference :comments, :category_comment, index: true, foreign_key: true
  end
end
