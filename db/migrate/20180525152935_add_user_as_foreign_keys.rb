class AddUserAsForeignKeys < ActiveRecord::Migration[5.2]
  def change
    add_reference :extracts, :created_by, references: :users, index: true
    add_foreign_key :extracts, :users, column: :created_by_id

    add_reference :extracts, :updated_by, references: :users, index: true
    add_foreign_key :extracts, :users, column: :updated_by_id


    add_reference :attestations, :created_by, references: :users, index: true
    add_foreign_key :attestations, :users, column: :created_by_id

    add_reference :attestations, :updated_by, references: :users, index: true
    add_foreign_key :attestations, :users, column: :updated_by_id
  end
end
