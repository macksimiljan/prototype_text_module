class AddCheckedUploadToExtracts < ActiveRecord::Migration[5.2]
  def change
    add_column :extracts, :checked_upload, :boolean, default: false, null: false
  end
end
