class ChangeDatatypeOfCoptic < ActiveRecord::Migration[5.2]
  def up
    Attestation.delete_all
    Extract.delete_all
    remove_column :extracts, :coptic
    add_column :extracts, :coptic, :xml
  end

  def down
    change_column :extracts, :coptic, :text
  end
end
