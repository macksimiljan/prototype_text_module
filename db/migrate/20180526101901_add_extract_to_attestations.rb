class AddExtractToAttestations < ActiveRecord::Migration[5.2]
  def change
    add_reference :attestations, :extract, index: true, foreign_key: true
  end
end
