class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.string :field
      t.text :body
      t.references :commentable, polymorphic: true, index: true

      t.timestamps
    end

    add_reference :comments, :created_by, references: :users, index: true
    add_foreign_key :comments, :users, column: :created_by_id

    add_reference :comments, :updated_by, references: :users, index: true
    add_foreign_key :comments, :users, column: :updated_by_id
  end
end
