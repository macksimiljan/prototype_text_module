class UpdateNullConstraints < ActiveRecord::Migration[5.2]
  def change
    change_column_null :attestations, :encoding, true
    change_column_null :extracts, :edition_position, false
  end
end
