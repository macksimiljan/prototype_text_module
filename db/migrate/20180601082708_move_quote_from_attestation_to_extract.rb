class MoveQuoteFromAttestationToExtract < ActiveRecord::Migration[5.2]
  def change
    remove_column :attestations, :unchecked_quote, :boolean
    remove_column :attestations, :quote_origin, :string
    remove_column :attestations, :quote_type, :string

    add_column :extracts, :unchecked_quote, :boolean, default: false
    add_column :extracts, :quote_origin, :string
    add_column :extracts, :quote_type, :string
  end
end
