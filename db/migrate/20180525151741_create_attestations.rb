class CreateAttestations < ActiveRecord::Migration[5.2]
  def change
    create_table :attestations do |t|
      t.boolean :unchecked_quote
      t.string :quote_origin
      t.string :quote_type
      t.string :orthography
      t.string :encoding

      t.timestamps
    end
  end
end
