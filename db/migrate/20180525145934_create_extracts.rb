class CreateExtracts < ActiveRecord::Migration[5.2]
  def change
    create_table :extracts do |t|
      t.text :coptic
      t.text :translation
      t.string :manuscript_position
      t.string :text_position
      t.string :edition_position

      t.timestamps
    end
  end
end
