source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'


### BASICS
gem 'rails', '~> 5.2.0'
gem 'pg', '>= 0.18', '< 2.0' # Use postgresql as the database for Active Record

### BACKEND
gem 'docx' # Interacting with .docx files


### FRONTEND
gem 'haml'
gem 'haml-rails', '~> 1.0'
gem 'bootstrap', '~> 4.1.1'
gem 'jquery-rails'
gem 'font-awesome-rails'
gem "bootstrap_form", ">= 4.0.0.alpha1"

### ASSETS
gem 'coffee-rails', '~> 4.2' # Use CoffeeScript for .coffee assets and views
gem 'jbuilder', '~> 2.5' # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'sass-rails', '~> 5.0' # Use SCSS for stylesheets
gem 'turbolinks', '~> 5' # Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'uglifier', '>= 1.3.0' # Use Uglifier as compressor for JavaScript assets


# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw] # Call 'byebug' anywhere in the code to stop execution and get a debugger console
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2' # Make rails development env snappier by avoiding reload on every request (instead OS flags for reload on file change)
  gem 'puma', '~> 3.11' # Use Puma as the app server
  gem 'spring' # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring-watcher-listen', '~> 2.0.0' # This gem makes Spring watch the filesystem for changes using Listen rather than by polling the filesystem.
  gem 'web-console', '>= 3.3.0' # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
end

group :test do
  gem 'capybara', '>= 2.15', '< 4.0' # Adds support for Capybara system testing and selenium driver
  gem 'chromedriver-helper' # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'selenium-webdriver' # Aims to mimic the behaviour of a real user, and as such interacts with the HTML of the application.
  gem 'rails-controller-testing'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
