require 'test_helper'

class ExtractsHelperTest < ActionView::TestCase

  test 'attestations are highlighted correctly' do
    text = 'abc^def^^g^i'
    expected_highlights = 'abc<span class="attestation-tag">^def^</span><span class="attestation-tag">^g^</span>i'
    actual_highlights = highlight_attestations text
    assert_equal expected_highlights, actual_highlights
  end
end