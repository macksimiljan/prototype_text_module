require 'test_helper'

class ExtractsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @extract = extracts(:one)
  end

  test "should get index" do
    get extracts_url
    assert_response :success
  end

  test "should get new" do
    get new_extract_url
    assert_response :success
  end

  test "should create extract" do
    assert_difference('Extract.count') do
      post extracts_url, params: { extract: { coptic: @extract.coptic, edition_position: @extract.edition_position, manuscript_position: @extract.manuscript_position, text_position: @extract.text_position, translation: @extract.translation } }
    end

    assert_redirected_to extract_url(Extract.last)
  end

  test "should show extract" do
    get extract_url(@extract)
    assert_response :success
  end

  test "should get edit" do
    get edit_extract_url(@extract)
    assert_response :success
    assert_includes assigns(:extract).coptic, "\n"
  end

  test "should update extract" do
    coptic = "\n<attestation id='1'>ⲡⲁ</attestation>ⲗⲓⲛ\n ⲁⲛ ϩⲁϥϫⲓⲧϥ | ⲛ̇ϭⲏ ⲡⲇⲓⲁ̇ⲃⲟⲗⲟⲥ ⲉ̇||ϫⲛ ⲟⲩⲧⲁⲩ ⲉϥϫⲁⲥⲉ | ⲉ̇ⲙⲁϣⲁ"
    patch extract_url(@extract), params: { extract: { coptic: coptic, edition_position: @extract.edition_position, manuscript_position: @extract.manuscript_position, text_position: @extract.text_position, translation: @extract.translation } }
    assert_redirected_to extract_url(@extract)
    @extract.reload
    assert_not_includes assigns(:extract).coptic, "\n"
    assert_equal @extract.attestations.first.orthography, 'ⲡⲁ'
  end

  test "no valid update of an extract" do
    coptic = "\n<attestation>ⲡⲁ</attestation>ⲗⲓⲛ\n ⲁⲛ ϩⲁϥϫⲓⲧϥ | ⲛ̇ϭⲏ ⲡⲇⲓⲁ̇ⲃⲟⲗⲟⲥ ⲉ̇||ϫⲛ ⲟⲩⲧⲁⲩ ⲉϥϫⲁⲥⲉ | ⲉ̇ⲙⲁϣⲁ"
    patch extract_url(@extract), params: { extract: { coptic: coptic, edition_position: @extract.edition_position, manuscript_position: @extract.manuscript_position, text_position: @extract.text_position, translation: @extract.translation } }
    assert_response :success
    @extract.reload
    assert_equal 'The following errors occured. Please fix them and try again: Unexpected tagging in Coptic field.', flash[:error]
    assert_equal @extract.attestations.first.orthography, 'ⲡⲁⲗⲓⲛ'
  end

  test "should destroy extract" do
    assert_difference('Extract.count', -1) do
      Extract.find(2).attestations.delete_all
      delete extract_url(Extract.find(2))
    end

    assert_redirected_to extracts_url
  end
end
