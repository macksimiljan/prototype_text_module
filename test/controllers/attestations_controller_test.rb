require 'test_helper'

class AttestationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @attestation = Attestation.find(1)
  end

  test "should get index" do
    get attestations_url
    assert_response :success
  end

  test "should get new" do
    get new_attestation_url, params: {extract_id: Extract.first.id}
    assert_response :success
  end

  test "should create attestation" do
    extract_coptic = "<attestation id='1'>ⲡⲁⲗⲓⲛ</attestation> ⲁⲛ ϩⲁϥϫⲓⲧϥ | ⲛ̇ϭⲏ ⲡⲇⲓⲁ̇ⲃⲟⲗⲟⲥ ⲉ̇||ϫⲛ ⲟⲩⲧⲁⲩ #ⲉϥϫⲁⲥⲉ# | ⲉ̇ⲙⲁϣⲁ"
    extract_translation = '<attestation>Again</attestation> the devil took him to a very #high# mountain'
    extract = Extract.find(1)
    number_of_attestations = extract.attestations.count
    params = { attestation: { orthography: 'ⲉϥϫⲁⲥⲉ', extract_id: extract.id, extract_coptic: extract_coptic, extract_translation: extract_translation } }

    post attestations_url, params: params

    extract.reload
    assert_redirected_to extract_url(extract)
    assert_equal 'ⲉϥϫⲁⲥⲉ', Attestation.last.orthography
    assert_equal number_of_attestations + 1, extract.attestations.count
    assert_equal "<attestation id='1'>ⲡⲁⲗⲓⲛ</attestation> ⲁⲛ ϩⲁϥϫⲓⲧϥ | ⲛ̇ϭⲏ ⲡⲇⲓⲁ̇ⲃⲟⲗⲟⲥ ⲉ̇||ϫⲛ ⲟⲩⲧⲁⲩ <attestation id='3'>ⲉϥϫⲁⲥⲉ</attestation> | ⲉ̇ⲙⲁϣⲁ", extract.coptic
  end

  test "should get edit" do
    get edit_attestation_url(@attestation)
    assert_response :success
  end

  test "should update attestation" do
    patch attestation_url(@attestation), params: { attestation: { encoding: @attestation.encoding, orthography: @attestation.orthography, extract_id: @attestation.extract_id } }
    assert_redirected_to extract_url(@attestation.extract)
  end

  test "should destroy attestation" do
    assert_difference('Attestation.count', -1) do
      delete attestation_url(@attestation)
    end

    assert_redirected_to extract_url(@attestation.extract)
  end
end
