require 'application_system_test_case'

class AttestationsTest < ApplicationSystemTestCase
  setup do
    @attestation = attestations(:one)
  end

  test 'visiting the index' do
    visit attestations_url
    assert_selector 'h1', text: 'Attestations'
  end

  test 'creating an attestation' do
    visit extract_path(Extract.first)
    click_on 'New Attestation'

    extract_coptic = "\r\n<attestation id='1'>ⲡⲁⲗⲓⲛ</attestation>\r\n ⲁⲛ ϩⲁϥϫⲓⲧϥ | ⲛ̇ϭⲏ ⲡⲇⲓⲁ̇ⲃⲟⲗⲟⲥ ⲉ̇||ϫⲛ #ⲟⲩⲧⲁⲩ# ⲉϥϫⲁⲥⲉ | ⲉ̇ⲙⲁϣⲁ"
    extract_translation = "\r\n<attestation>Again</attestation>\r\n the devil took him to a very high #mountain#"
    fill_in 'attestation_extract_coptic', with: extract_coptic
    fill_in 'attestation_extract_translation', with: extract_translation
    click_on 'Extract Orthography'
    fill_in 'attestation_encoding', with: 'nn-42-b'
    click_on 'Create Attestation'

    assert_text 'Attestation was successfully created'
  end
end
