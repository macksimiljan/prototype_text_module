require "test_helper"

# see https://www.ynonperek.com/2017/08/14/tutorial-writing-a-first-rails-system-test/
class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
end
