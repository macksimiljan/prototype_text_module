require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'only a valid user can be created' do
    previous_count = User.count
    User.create(code: 'muster')
    assert_equal previous_count, User.count

    User.create(code: 'MuSt')
    assert_equal previous_count + 1, User.count
  end
end
