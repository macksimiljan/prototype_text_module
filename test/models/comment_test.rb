require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test 'only a valid comment can be created' do
    previous_count = Comment.count
    assert_raise do
      Comment.create!(field: 'coptic', created_by: User.first, updated_by: User.first,
                      category_comment: CategoryComment.first, commentable: Extract.first)
    end
    Comment.create!(field: 'coptic', body: 'comment', created_by: User.first, updated_by: User.first,
                    category_comment: CategoryComment.first, commentable: Extract.first, category_comment_id: 1)
    assert_equal previous_count + 1, Comment.count
  end

  test 'access of commentabel and comment is possible' do
    assert_equal Attestation, Comment.first.commentable.class
    assert_equal 1, Comment.first.commentable.id

    assert_equal 2, Extract.find(2).comments.count
    assert_equal ['coptic', 'translation'], Extract.find(2).comments.order(:field).pluck(:field)
  end
end
