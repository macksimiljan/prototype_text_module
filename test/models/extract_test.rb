require 'test_helper'

class ExtractTest < ActiveSupport::TestCase
  test 'ony a valid extract can be created' do
    previous_count = Extract.count
    Extract.create(translation: 'Foo bar!')
    assert_equal previous_count, Extract.count
    Extract.create(coptic: 'foos baros.', edition_position: '11', created_by: User.first, updated_by: User.first)
    assert_equal previous_count + 1, Extract.count
  end
end
