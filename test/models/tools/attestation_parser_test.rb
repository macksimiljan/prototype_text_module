require 'test_helper'

class AttestationParserTest < ActiveSupport::TestCase

  test 'parsing valid attestations works as expected' do
    coptic = 'abc^def^ ghi^jk^l'
    translation = '^yy^ xxxxxxx^z^'
    extract = Extract.create!(coptic: coptic,
                              translation: translation,
                              created_by: User.first,
                              updated_by: User.first,
                              edition_position: '42')
    previous_attestations_count = Attestation.count

    parser = Tools::AttestationParser.new(extract.id)
    parser.process

    assert_equal previous_attestations_count + 2, Attestation.count
    assert_equal parser.failed_attestations.count, 0
    assert_equal Attestation.last.created_by, User.first
    assert_equal Attestation.last.extract, extract
    assert_equal Attestation.last.orthography, 'jk'
    assert_equal Attestation.second_to_last.orthography, 'def'

    extract.reload
    ids = [Attestation.last.id - 1, Attestation.last.id]
    assert_equal "abc<attestation id='#{ids.first}'>def</attestation> ghi<attestation id='#{ids.second}'>jk</attestation>l", extract.coptic
    assert_equal "<attestation>yy</attestation> xxxxxxx<attestation>z</attestation>", extract.translation
  end

  test 'parsing invalid attestations works as expected' do
    coptic = 'abc^def^ ghi^^jk^l'
    translation = '^yy^ xxxxxxx^z'
    extract = Extract.create!(coptic: coptic,
                              translation: translation,
                              created_by: User.first,
                              updated_by: User.first,
                              edition_position: '42')
    previous_attestations_count = Attestation.count

    parser = Tools::AttestationParser.new(extract.id)
    parser.process

    assert_equal previous_attestations_count + 1, Attestation.count
    assert_equal parser.failed_attestations.count, 1
    assert_equal parser.failed_attestations.first.orthography, ''

    extract.reload
    assert_equal "abc<attestation id='3'>def</attestation> ghijkl", extract.coptic
    assert_equal "<attestation>yy</attestation> xxxxxxxz", extract.translation
  end
end