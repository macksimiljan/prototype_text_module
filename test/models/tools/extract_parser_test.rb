require 'test_helper'

class ExtractParserTest < ActiveSupport::TestCase

  test 'parsing extracts of valid user works as expected' do
    extract_file = file_fixture 'extracts_test0.docx'

    previous_extracts_count = Extract.count

    parser = Tools::ExtractParser.new(extract_file)
    parser.read_and_process

    assert_equal previous_extracts_count + 7, Extract.count
    assert_equal Extract.last.created_by.code, 'JoDo'
    assert_equal parser.failed_rows.count, 1
    error = assert_raises ActiveRecord::RecordInvalid do
      parser.failed_rows.first.save!
    end
    assert_equal "Validation failed: Edition position can't be blank", error.message
  end

  test 'a user is only created once' do
    extract_file = file_fixture 'extracts_test0.docx'

    parser = Tools::ExtractParser.new(extract_file)
    parser.read_and_process

    assert_equal 1, User.where(code: 'JoDo').count
  end

  test 'parsing extracts of invalid user throws exception' do
    extract_file = file_fixture 'extracts_test1.docx'

    previous_extracts_count = Extract.count

    parser = Tools::ExtractParser.new(extract_file)
    assert_raises ActiveRecord::RecordInvalid do
      parser.read_and_process
    end

    assert_equal previous_extracts_count, Extract.count
  end

  test 'attestations are parsed for a valid extract' do
    extract_file = file_fixture 'extracts_test0.docx'

    parser = Tools::ExtractParser.new(extract_file)
    parser.read_and_process

    assert_equal 1, Extract.last.attestations.count
    assert_equal 'ⲁⲗⲗ(ⲟⲥ)/', Extract.last.attestations.first.orthography
  end
end