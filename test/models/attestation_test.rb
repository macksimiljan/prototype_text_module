require 'test_helper'

class AttestationTest < ActiveSupport::TestCase
  test 'only a valid attestation can be created' do
    previous_count = Attestation.count
    assert_raise do
      Attestation.create!(encoding: 'foo',
                          extract: Extract.first,
                          created_by: User.first, updated_by: User.first)
    end
    assert_equal previous_count, Attestation.count

    Attestation.create(encoding: 'foo', orthography: 'bar',
                       extract: Extract.first,
                       created_by: User.first, updated_by: User.first)
    assert_equal previous_count + 1, Attestation.count
  end
end
