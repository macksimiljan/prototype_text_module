module Tools
  # Parses a docx file containing extracts
  class ExtractParser

    attr_reader :failed_rows

    def initialize(file_path)
      @file_path = file_path
      @failed_rows = []
      @inserted_extracts = []
    end

    def read_and_process
      raise Exceptions::InvalidExtractFile unless File.exist? @file_path

      doc = Docx::Document.open @file_path
      process_document doc
      @inserted_extracts
    end

    private

    def process_document(doc)
      valid? doc

      meta_table = doc.tables.first
      extracts_table = doc.tables.second

      process_meta_information meta_table

      rows_without_header = extracts_table.rows[1..-1]
      rows_without_header.each do |row|
        process_row row
      end
    end

    def process_meta_information(table)
      values_row = table.rows.second
      lexicographer = process_cell values_row.cells.first
      create_user_if_not_exists lexicographer
    end

    def create_user_if_not_exists(lexicographer)
      @user = User.find_or_create_by!(code: lexicographer)
    end

    def process_row(row)
      coptic = process_cell row.cells.first
      translation = process_cell row.cells.second
      edition_position = process_cell row.cells.third

      maybe_create_extract_and_attestations coptic, translation, edition_position
    end

    def process_cell(cell)
      cell.text.blank? ? nil : cell.text.squish
    end

    def maybe_create_extract_and_attestations(coptic, translation, edition_position)
      extract = Extract.new(coptic: coptic,
                            translation: translation,
                            edition_position: edition_position,
                            created_by: @user,
                            updated_by: @user)
      if extract.save
        @inserted_extracts << extract.id
        attestation_parser = Tools::AttestationParser.new(extract.id)
        attestation_parser.process
      else
        @failed_rows << extract unless extract.save
      end
    end

    def valid?(doc)
      raise Exceptions::InvalidExtractFile, 'File must contain two tables.' unless doc.tables.count == 2
      meta_table = doc.tables.first
      raise Exceptions::InvalidExtractFile, 'The first table of the file is ill-formed.' unless meta_table.rows.count == 2 and meta_table.columns.count == 1
      extracts_table = doc.tables.second
      raise Exceptions::InvalidExtractFile, 'The second table of the file is ill-formed.' unless extracts_table.columns.count == 3
      true
    end
  end
end