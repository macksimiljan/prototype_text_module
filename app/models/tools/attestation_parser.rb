module Tools
  class AttestationParser
    attr_reader :failed_attestations

    def initialize(extract_id)
      @extract = Extract.find extract_id
      @coptic = ''
      @translation = ''
      @orthographies = []
      @attestation_ids = []
      @failed_attestations = []

      @current_orthography = ''
      @is_attestation = false
    end

    def process
      process_coptic
      process_translation
    end

    private

    def process_coptic
      @extract.coptic.each_char do |char|
        if char.eql? '^'
          process_tag
        elsif @is_attestation
          @current_orthography += char
        else
          @coptic += char
        end
      end

      @coptic += @current_orthography
      @extract.update(coptic: @coptic)
    end

    def process_tag
      if @is_attestation
        attestation_id = create_attestation
        tagged_orthography = "<attestation id='#{attestation_id}'>#{@current_orthography}</attestation>"
        @coptic += attestation_id.present? ? tagged_orthography : @current_orthography

        @current_orthography = ''
        @is_attestation = false
      else
        @is_attestation = true
      end
    end

    def create_attestation
      a = Attestation.new(
        orthography: @current_orthography,
        created_by: @extract.created_by,
        updated_by: @extract.created_by,
        extract: @extract
      )
      @failed_attestations << a unless a.save
      @attestation_ids << a.id
      a.id
    end

    def process_translation
      return unless @extract.translation.present?

      @is_attestation = false
      @current_orthography = ''
      @extract.translation.each_char do |char|
        if char.eql? '^'
          process_translation_tag
        elsif @is_attestation
          @current_orthography += char
        else
          @translation += char
        end
      end

      @translation += @current_orthography
      @extract.update(translation: @translation)
    end

    def process_translation_tag
      if @is_attestation
        tagged_orthography = "<attestation>#{@current_orthography}</attestation>"
        @translation += tagged_orthography

        @current_orthography = ''
        @is_attestation = false
      else
        @is_attestation = true
      end
    end
  end
end
