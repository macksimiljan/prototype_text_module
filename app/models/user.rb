class User < ApplicationRecord
  validates :code, presence: true, uniqueness: { case_sensitive: false },format: {with: /(\A[A-ZÄÖÜ][a-zäöüß][A-ZÄÖÜ][a-zäöüß])\z|(\AAdmin\z)/}
end
