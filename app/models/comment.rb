class Comment < ApplicationRecord
  belongs_to :created_by, class_name: 'User'
  belongs_to :updated_by, class_name: 'User'

  belongs_to :commentable, polymorphic: true
  belongs_to :category_comment

  validates :body, presence: true, length: { in: 3..1000 }
end
