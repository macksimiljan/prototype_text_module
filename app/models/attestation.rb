class Attestation < ApplicationRecord
  belongs_to :created_by, class_name: 'User'
  belongs_to :updated_by, class_name: 'User'

  belongs_to :extract
  has_many :comments, as: :commentable

  validates :orthography, presence: true
end
