class Extract < ApplicationRecord
  belongs_to :created_by, class_name: 'User'
  belongs_to :updated_by, class_name: 'User'

  has_many :attestations
  has_many :comments, as: :commentable

  validates :coptic, presence: true
  validates :edition_position, presence: true
  validate :coptic_is_tagged_correctly, on: [:create, :update]

  before_save :remove_line_breaks

  def add_line_breaks
    substitutions = [['<attestation', "\n<attestation"],
                     ['</attestation>', "</attestation>\n"]]
    substitutions.each do |s|
      coptic.gsub! s.first, s.second
      translation.gsub! s.first, s.second
    end
    self
  end

  private

  def remove_line_breaks
    coptic.delete! "\r\n"
    translation&.delete! "\r\n"
  end

  def coptic_is_tagged_correctly
    all_attestations_present = attestations.count > 0 ? attestations.map {|a| coptic.include? "<attestation id='#{a.id}'>"}.reduce(:&) : true
    coptic_as_xml = '<root>' + coptic + '</root>' if coptic.present?
    well_formed = Nokogiri::XML(coptic_as_xml).errors.none?

    unless all_attestations_present && well_formed
      errors.add :base, 'Unexpected tagging in Coptic field.'
    end
  end
end
