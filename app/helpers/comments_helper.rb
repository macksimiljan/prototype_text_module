module CommentsHelper
  def comment_button(target, label='')
    content_tag :button, class: 'btn btn-default btn-add-comment', type: 'button', 'data-toggle': 'collapse', 'data-target':target do
      fa_icon('comment-o') + label
    end
  end

  def toggle_please_check_button(object, field)
    button_class =  object.comments.where(field: field, category_comment: CategoryComment.where(name: 'fast please check')).any? ? 'value-active' : 'value-deactive'
    path = toggle_please_check_comments_path params: {field: field, commentable_type: object.class.name, commentable_id: object.id}
    link_to fa_icon('clipboard', class: button_class), path
  end

  def delete_comment_button(comment)
    link_to comment_path(comment), method: :delete, data: {confirm: 'Are you sure?'} do
      fa_icon('trash', class: 'a-inverse')
    end
  end
end