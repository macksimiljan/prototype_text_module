module ExtractsHelper
  def highlight_attestations(text)
    return text if text.nil? || text.index('^').nil?

    opening_tag = '<span class="attestation-tag">'
    closing_tag = '</span>'
    text_mod = ''
    is_open = false
    text.each_char do |char|
      if char.eql?('^') && is_open
        text_mod += char + closing_tag
        is_open = false
      elsif char.eql?('^') && !is_open
        text_mod += opening_tag + char
        is_open = true
      else
        text_mod += char
      end
    end
    text_mod
  end

  def wrong_tag_count_note(extract)
    odd_tag_number_in_coptic = extract.coptic.count('^').odd?
    odd_tag_number_in_transl = extract.translation.try { count('^').odd? }

    return unless odd_tag_number_in_coptic || odd_tag_number_in_transl

    where = []
    where << 'Coptic Text' if odd_tag_number_in_coptic
    where << 'Translation' if odd_tag_number_in_transl

    haml_tag :div, class: 'text-danger' do
      haml_concat fa_icon('exclamation-triangle')
      haml_concat "Odd Number of Tags in #{where.join(' and ')}"
    end
  end

  def unchecked_upload_note(extract, options={})
    note_class = options.fetch(:class, '')
    if extract.checked_upload
      haml_tag :div, class: 'text-success ' + note_class do
        haml_concat fa_icon('check')
        haml_concat 'Checked Upload'
      end
    else
      haml_tag :div, class: 'text-danger ' + note_class do
        haml_concat fa_icon('exclamation-triangle')
        haml_concat 'Unchecked Upload'
      end
    end
  end

end
