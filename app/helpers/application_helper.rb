module ApplicationHelper
  def optional_attribute(value)
    value.present? ? value : '-'
  end

  def user_info_tooltip(object, options={})
    title = "created: #{object.created_by.code}, #{object.created_at.strftime('%b %d, %Y')}"
    title += "\nupdated: #{object.updated_by.code}, #{object.updated_at.strftime('%b %d, %Y')}"
    class_value = ('text-muted ' + options.fetch(:class, '')).squish
    content_tag 'has-tooltip', title: title do
      fa_icon('info-circle', class: class_value)
    end
  end

  def errors_for(object)
    if object.errors.any?
      content_tag(:div, class: "card border-danger") do
        concat(content_tag(:div, class: "card-header bg-danger text-white") do
          concat "#{pluralize(object.errors.count, "error")} prohibited this #{object.class.name.downcase} from being saved:"
        end)
        concat(content_tag(:ul, class: 'mb-0 list-group list-group-flush') do
          object.errors.full_messages.each do |msg|
            concat content_tag(:li, msg, class: 'list-group-item')
          end
        end)
      end
    end
  end
end
