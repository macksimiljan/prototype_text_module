# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.coffee.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->

  $('#extract_orthography').click ->
    parseAndCheckNewOrthography()

  $('#clear_orthography').click ->
    setCopticAndTranslationToOriginalValues()


parseAndCheckNewOrthography = ->
  hideErrorMessage()
  coptic = $('#attestation_extract_coptic').val()
  if coptic.indexOf('#') < 0
    showErrorMessage 'Use "#" for tagging the new orthography.'
    return false

  previous_coptic = $('#attestation_extract_coptic_original').val()
  if previous_coptic != coptic.replace(/#/g, '')
    showErrorMessage 'You are only allowed to use "#". Do not change the extract otherwise.'
    return false

  # ab#cd#ef#gh# --> [ab, cd, ef, gh]
  split_coptic = coptic.split('#')
  number_of_tags = split_coptic.length - 1
  if number_of_tags % 2 == 1
    showErrorMessage "The number of tags for the new attestation must be even. Currently, it is #{number_of_tags}."
    return false

  indices = [0...split_coptic.length].filter (i) -> i % 2 == 1
  new_orthography_parts = (split_coptic[i] for i in indices)
  new_orthography = new_orthography_parts.join(' ... ')
  unless new_orthography.indexOf('<attestation') < 0 || new_orthography.indexOf('</attestation>') < 0
    showErrorMessage 'You are not allowed to have a part of an existing orthography inside the new one.'
    return false

  $('#attestation_orthography').val(new_orthography)


setCopticAndTranslationToOriginalValues = ->
  hideErrorMessage()
  $('#attestation_orthography').val('')
  coptic = $('#attestation_extract_coptic_original').val()
  $('#attestation_extract_coptic').val(coptic)
  translation = $('#attestation_extract_translation_original').val()
  $('#attestation_extract_translation').val(translation)


showErrorMessage = (message) ->
  $('#extract-error-message').text(message)
  $('#extract-errors').css('visibility', 'visible');


hideErrorMessage = ->
  $('#extract-errors').css('visibility', 'hidden');