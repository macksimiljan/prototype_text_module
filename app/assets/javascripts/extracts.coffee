$(document).on "turbolinks:load", ->

  $('#extract_coptic').click ->
    selectTag $(this)

  $('#extract_translation').click ->
    selectTag $(this)

  selectTag = (element) ->
    start = element.prop('selectionStart')
    end = element.prop('selectionEnd')
    if start == end
      next_gt = element.val().indexOf('>', start)
      next_lt = element.val().indexOf('<', start)
      last_lt = element.val().lastIndexOf('<', start)
      if next_gt < next_lt || (next_gt > 0 && next_lt < 0)
        element.selectRange(last_lt, next_gt + 1)

  # https://gist.github.com/beiyuu/2029907
  $.fn.selectRange = (start, end) ->
    e = this[0]
    return unless e

    if e.setSelectionRange # WebKit
      e.focus()
      e.setSelectionRange start, end

    else if e.createTextRange # IE
      range = e.createTextRange()
      range.collapse true
      range.moveEnd   'character', end
      range.moveStart 'character', start
      range.select()

    else if e.selectionStart
      e.selectionStart = start
      e.selectionEnd   = end

