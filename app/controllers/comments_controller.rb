class CommentsController < ApplicationController

  def index
    @comments = Comment.all.order(:category_comment_id, :commentable_type, :field)
  end

  def new
    @comment = Comment.new
  end

  def create
    current_user = User.find_by_code('Admin')
    @comment = Comment.new(comment_params.merge(created_by: current_user, updated_by: current_user))

    if @comment.save
      flash[:success] = 'Comment saved.'
    else
      flash[:error] = 'Comment could not be saved.'
    end
    redirect_back fallback_location: root_path
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.delete
    redirect_back fallback_location: root_path
  end

  def toggle_please_check
    commentable_id = params[:commentable_id]
    commentable_type = params[:commentable_type]
    field = params[:field]
    current_user = User.find_by_code('Admin')
    category_comment = CategoryComment.find_by_name 'fast please check'
    unique_params = {category_comment: category_comment, commentable_id: commentable_id, commentable_type: commentable_type, field: field}
    body = '<i>fast please check</i>'
    if Comment.exists?(unique_params)
      Comment.where(unique_params).delete_all
    else
      Comment.create!(category_comment: category_comment, commentable_id: commentable_id,
                      commentable_type: commentable_type, created_by: current_user, updated_by: current_user,
                      body: body, field: field)
    end
    redirect_back fallback_location: root_path
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params.require(:comment).permit(:field, :body, :commentable_id, :commentable_type, :category_comment_id)
  end
end
