class DashboardsController < ApplicationController

  def text_module
    # goes to text module
  end

  def upload_and_parse
    uploaded_io = params[:file]

    begin
      inserted_extracts = save_and_parse_file uploaded_io
    rescue StandardError => e
      redirect_to text_dashboard_path, flash: { error: "An error occurred: #{e.message}" }
      return
    end

    redirect_to root_path, flash: { success: "#{inserted_extracts.count} extracts inserted!" }
  end

  private

  def save_and_parse_file(uploaded_io)
    raise Exceptions::InvalidExtractFile, 'No file selected!' if uploaded_io.nil?

    save_path = unique_save_path uploaded_io.original_filename
    File.open(save_path, 'wb') do |file|
      file.write(uploaded_io.read)
    end
    Tools::ExtractParser.new(save_path).read_and_process
  end

  def unique_save_path(filename)
    parts = filename.split('.')
    parts[-2] = "#{parts[-2]}#{Time.now.to_i}"
    parts = parts[0...-1] << parts[-1]
    filename = parts.join('.')
    directory = Rails.root.join('storage', 'extracts')
    FileUtils.mkdir_p(directory) unless File.directory? directory
    Rails.root.join('storage', 'extracts', filename)
  end

end