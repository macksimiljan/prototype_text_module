class AttestationsController < ApplicationController
  before_action :set_attestation, only: [:edit, :update, :destroy]

  # GET /attestations
  # GET /attestations.json
  def index
    @attestations = Attestation.all
  end

  # GET /attestations/new
  def new
    new_attestation_for_extract
  end

  # GET /attestations/1/edit
  def edit
  end

  # POST /attestations
  def create
    extract_coptic = params[:attestation].delete(:extract_coptic)
    extract_translation = params[:attestation].delete(:extract_translation)

    current_user = User.find_by_code('Admin')
    @attestation = Attestation.new(attestation_params.merge(created_by: current_user, updated_by: current_user))

    respond_to do |format|
      if @attestation.save
        while extract_coptic.include? '#' do
          extract_coptic.sub! '#', "<attestation id='#{@attestation.id}'>"
          extract_coptic.sub! '#', '</attestation>'
        end
        while extract_translation.include? '#' do
          extract_translation.sub! '#', '<attestation>'
          extract_translation.sub! '#', '</attestation>'
        end
        @attestation.extract.update(coptic: extract_coptic, translation: extract_translation)
        format.html { redirect_to extract_path(@attestation.extract), flash: {success: 'Attestation was successfully created.'} }
      else
        flash.now[:error] = "The following errors occurred. Please fix them and try again:\n #{@attestation.errors.full_messages.join('. ')}."
        format.html { render :new  }
      end
    end
  end

  # PATCH/PUT /attestations/1
  def update
    respond_to do |format|
      if @attestation.update(attestation_params)
        format.html { redirect_to extract_path(@attestation.extract), flash: {success: 'Attestation was successfully updated.'} }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /attestations/1
  def destroy
    extract = @attestation.extract
    @attestation.destroy
    delete_attestation_from_coptic @attestation, extract
    respond_to do |format|
      format.html { redirect_to extract_url(extract), flash: {success: 'Attestation was successfully destroyed.'} }
    end
  end

  private

  def new_attestation_for_extract
    extract_id = params.fetch(:extract_id, nil)
    @attestation = Attestation.new(extract_id: extract_id)
  end

  def delete_attestation_from_coptic(attestation, extract)
    coptic = extract.coptic
    parts = attestation.orthography.split(' ... ')
    parts.each do |p|
      attestation_node = "<attestation id='#{attestation.id}'>#{p}</attestation>"
      coptic.sub! attestation_node, p
    end
    extract.update(coptic: coptic)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_attestation
    @attestation = Attestation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def attestation_params
    params.require(:attestation).permit(:orthography, :encoding, :extract_id, :created_by_id, :updated_by_id)
  end
end
