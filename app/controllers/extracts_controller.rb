class ExtractsController < ApplicationController
  before_action :set_extract, only: [:show, :edit, :update, :destroy]

  # GET /extracts
  # GET /extracts.json
  def index
    @extracts = Extract.all.order(checked_upload: :desc, id: :asc)
  end

  # GET /extracts/1
  # GET /extracts/1.json
  def show
  end

  # GET /extracts/new
  def new
    @extract = Extract.new
  end

  # GET /extracts/1/edit
  def edit
    @extract.add_line_breaks
  end

  # POST /extracts
  def create
    creator = User.find_by_code('Admin')
    @extract = Extract.new(extract_params.merge({created_by: creator, updated_by: creator}))

    respond_to do |format|
      if @extract.save
        format.html { redirect_to @extract, flash: {success: 'Extract was successfully created.'} }
      else
        flash.now[:error] = "The following errors occured. Please fix them and try again: #{@extract.errors.full_messages.join('. ')}"
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /extracts/1
  def update
    attestations = @extract.attestations
    respond_to do |format|
      if @extract.update(extract_params)
        attestations.each do |attestation|
          parts = Nokogiri::HTML(@extract.coptic).xpath("//attestation[@id='#{attestation.id}']/text()").map(&:content)
          attestation.update(orthography: parts.join(' ... '))
        end
        format.html { redirect_to @extract, flash: {success: 'Extract was successfully updated.'} }
      else
        flash.now[:error] = "The following errors occured. Please fix them and try again: #{@extract.errors.full_messages.join('. ')}"
        format.html { render :edit }
      end
    end
  end

  # DELETE /extracts/1
  def destroy
    @extract.destroy
    respond_to do |format|
      format.html { redirect_to extracts_url, flash: {sucess: 'Extract was successfully destroyed.'} }
    end
  end

  def check_upload
    set_extract
    @extract.update(checked_upload: true)
    redirect_back fallback_location: root_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_extract
    @extract = Extract.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def extract_params
    params.require(:extract).permit(:coptic, :translation,
                                    :manuscript_position, :text_position, :edition_position,
                                    :unchecked_quote, :quote_origin, :quote_type, :checked_upload)
  end
end
