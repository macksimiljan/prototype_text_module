class UsersController < ApplicationController

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to users_url, flash: {success: 'User was successfully created.'}
    else
      flash.now[:error] = "The following errors occurred. Please fix them and try again:\n #{@user.errors.full_messages.join('. ')}."
      render :new
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:code)
  end
end
